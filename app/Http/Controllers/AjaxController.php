<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function send_mail(Request $request) {
        header('Access-Control-Allow-Origin: *');
        return $request->to;

        /*
        fetch("http://localhost:8000/api/send-mail", {
    method: 'post',
	mode: 'cors', 
	headers: new Headers({
		
		'Accept': 'application/json',
	}),
    body: JSON.stringify({
		"from": "samuel@devexteam.com",
		"to": "max.tec92@hotmail.com",
		"message": "Hola mundo"
	})
  })
	.then(function(response) {
    	console.log(response.json());
  })
	.catch(err => console.log(err)); */
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
